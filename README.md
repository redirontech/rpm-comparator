# README #



### What is this repository for? ###



This repository is used for comparing two different directories and find out the greatest and latest rpms which will help the users to insatll in the server.

This repository consists of two files named 1)history_manager.sh 2)compare_utility.sh

These 2 files should keep in scripts folder.

### How do I get set up? ###

1) history_manager.sh 

This script file accept 2 input parameter as source and destiation directory. Generelly the source directory which contains the latest rpms. Those files will be copied to new history folder for tracking purpose. While copying the files, it will still keeps the attributes like timestamp, ownership etc.  The desitination folder name is unique and timstamp based.

Note: if there is no input argument , script takes some deafult path

* $ sh history_manager.sh /rpms/916/dev/ /rpms/916/history/dev/ 

2) compare_utility.sh

This script file accept 2 input parameter as  Directory1 and Directory2.

* $ sh compare_utility.sh /rpms/916/history/dev/20-02-2020-19-58-26/dev/core /rpms/916/history/dev/20-02-2020-21-52-31/dev/core

Please note that, the Directory2 expects the very latest files. Once execute the script, it creates compare.txt file which contains , the files that are in Directory2 but not in Directory1. If both Directory1 and Directory2 having same file , then there is a timstamp based check as well and result will append to the compare.txt.


### Are the above scripts can run from other scripts? ###

* These scripts can be run from other script as well. 

