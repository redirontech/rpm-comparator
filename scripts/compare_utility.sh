Dir1=/rpms/916/history/dev/20-02-2020-19-58-26/dev/core
Dir2=/rpms/916/history/dev/20-02-2020-21-52-31/dev/core
if [ $# -eq 2 ]
  then
    Dir1=$1
    Dir2=$2
    echo "Compared $Dir1 & $Dir2" > $Dir2/compare.txt
    echo "*Script compared $Dir1 & $Dir2 and result stored in compare.txt*"
  else
    echo "*No user input.So script takes default path as $Dir1 & $Dir2 and result stored in compare.txt*"
    echo "User not provided enough arguments. So script takes default path as $Dir1 & $Dir2" > $Dir2/compare.txt
fi
#find  "$Dir1/" "$Dir1/" "$Dir2/" -printf '%P\n' | sort | uniq -u
echo "---Following are having changes or newly added----" >> $Dir2/compare.txt
for file in $Dir2/*; do
    name=${file##*/}
    if [[ -f $Dir1/$name ]]; then
        #echo "$name exists in both directories"
                file1=$Dir1/$name
                file2=$Dir2/$name
                if [ $file2 -nt $file1 ]; then
                        echo "$name" >> $Dir2/compare.txt
                fi
    else
        echo "$name" >> $Dir2/compare.txt
    fi
done
